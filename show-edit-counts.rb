#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

load './mwapi.rb'
require 'yaml'

mw = MWApi.new('https://wiki.parabolagnulinux.org/api.php')
credentials = YAML.load_file('credentials.yml')
mw.login(credentials['username'], credentials['password'])

userqueue = []

aufrom = ''
while not aufrom.nil? do
	audata = mw.query(
		:list => :allusers,
		:auprop => 'blockinfo|editcount|registration',
		:aulimit => 5000,
		:aufrom => aufrom)
	userqueue.concat(audata['query']['allusers'])
	aufrom = (audata['query-continue'].nil?) ? nil : audata['query-continue']['allusers']['aufrom']
end

userqueue.each_slice(500) do |userlist|
	contribcount = {}
	uccontinue = ''
	while not uccontinue.nil?
		query = {
			:list => :usercontribs,
			:ucuser => (userlist.map{|u| u['name']}).join('|'),
			:uclimit => 5000,
		}
		if uccontinue.length > 0
			query.merge!({:uccontinue => uccontinue})
		end
		ucdata = mw.query(query)
		ucdata['query']['usercontribs'].each do |contrib|
			contribcount[contrib['user']] ||= 0
			contribcount[contrib['user']] += 1
		end
		uccontinue = (ucdata['query-continue'].nil?) ? nil : ucdata['query-continue']['usercontribs']['uccontinue']
	end
	userlist.each do |user|
		existing_editcount = contribcount[user['name']] || 0
		print "name: #{user['name']}\tregistration: #{user['registration']}\ttotal_editcount: #{user['editcount']}\texisting_editcount: #{existing_editcount}"
		if user['blockid'].nil?
			print "\n"
		else
			print "\tblockreason: #{user['blockreason']}\n"
		end
	end
end
