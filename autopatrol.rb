#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

load 'mwapi.rb'
require 'yaml'

mw = MWApi.new('https://wiki.parabolagnulinux.org/api.php')
credentials = YAML.load_file('credentials.yml')
mw.login(credentials['username'], credentials['password'])

print "Finding autopatrol users...\n"
users = []
aufrom = ''
while not aufrom.nil? do
	audata = mw.query(
		:list => :allusers,
		:aurights => :autopatrol,
		:aulimit => 5000,
		:aufrom => aufrom)
	users.concat(audata['query']['allusers'])
	aufrom = (audata['query-continue'].nil?) ? nil : audata['query-continue']['allusers']['aufrom']
end

print "Finding unpatrolled edits by autopatrol users...\n"
changes = []
users.each do |user|
	print " -> #{user['name']}\n"
	rccontinue = ''
	while not rccontinue.nil?
		query = {
			:list => :recentchanges,
			:rcuser => user['name'],
			:rclimit => 5000,
			:rcshow => '!patrolled',
			:rctoken => :patrol,
		}
		if rccontinue.length > 0
			query.merge!({:rccontinue => rccontinue})
		end
		rcdata = mw.query(query)
		changes.concat(rcdata['query']['recentchanges'])
		rccontinue = (rcdata['query-continue'].nil?) ? nil : rcdata['query-continue']['recentchanges']['rccontinue']
	end
end

changes.each do |change|
	print "Patrolling change #{change['rcid']} to #{change['title']}\n"
	mw.patrol({:rcid => change['rcid'], :token => change['patroltoken']})
end
