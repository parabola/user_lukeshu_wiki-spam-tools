#!/bin/bash

rm -rf user-pages.cache.bak
mkdir -p user-pages.cache
mv user-pages.cache user-pages.cache.bak
mkdir user-pages.cache

./user-pages-list.rb |
while read -r title; do
	if ! [[ -f "user-pages.cache/$title" ]]; then
		if [[ -f "user-pages.cache.bak/$title" ]]; then
			cp -v "user-pages.cache.bak/$title" "user-pages.cache/$title"
		else
			wget -O "user-pages.cache/$title" "https://wiki.parabolagnulinux.org/index.php?title=${title}&action=raw"
		fi
	fi
done
