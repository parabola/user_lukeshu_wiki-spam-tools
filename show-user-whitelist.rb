#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

load 'mwapi.rb'
require 'yaml'

mw = MWApi.new('https://wiki.parabolagnulinux.org/api.php')
credentials = YAML.load_file('credentials.yml')
mw.login(credentials['username'], credentials['password'])

users = []
aufrom = ''
while not aufrom.nil? do
	audata = mw.query(
		:list => :allusers,
		:aurights => :autopatrol,
		:aulimit => 5000,
		:aufrom => aufrom)
	users.concat(audata['query']['allusers'])
	aufrom = (audata['query-continue'].nil?) ? nil : audata['query-continue']['allusers']['aufrom']
end

users.each do |user|
	print "#{user['name']}\n"
end
