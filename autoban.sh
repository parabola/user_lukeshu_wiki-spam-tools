#!/bin/bash

cached=false
if [[ $1 == --cached ]]; then
	cached=true
fi

for file in show-user-whitelist show-edit-counts; do
	if [[ ! -f "${file}.txt" ]] || ! $cached; then
		echo "Updating ${file}.txt"
		./${file}.rb > ${file}.txt
	fi
done

comm -13 \
	<(sort show-user-whitelist.txt) \
	<(< show-edit-counts.txt \
		grep -v 'blockreason:' |
		grep -v 'total_editcount: 0\b' |
		grep 'existing_editcount: 0\b' |
		cut -d $'\t' -f1 | cut -d ' ' -f2- |
		sort) |
./block-users.rb "Spammer (Autodetected: has made edits, but only to deleted pages)"
