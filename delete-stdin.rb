#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

load 'mwapi.rb'
require 'yaml'

mw = MWApi.new('https://wiki.parabolagnulinux.org/api.php')
credentials = YAML.load_file('credentials.yml')
mw.login(credentials['username'], credentials['password'])

file = File.open("/dev/stdin", "rb")
contents = file.read
titles = contents.split("\n")
titles.each_slice(500) do |list|
	mw.delete_by_title(list, { :reason => ARGV.join(' ') });
end
