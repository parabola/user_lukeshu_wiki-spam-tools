#!/usr/bin/env ruby
require 'rest_client'
require 'json'
#require 'open-uri'

class MWApi
	def initialize(api)
		@api = api
		@cookies = {}
	end
	def cookies
		@cookies
	end

	def get(params = {})
		params = params.merge({ :format => :json})
		response = RestClient.get(@api, { :params => params, :cookies => @cookies })
		@cookies.merge!(response.cookies)
		JSON.parse(response)
	end
	def post(params = {}, payload = '')
		params = params.merge({ :format => :json})
		response = RestClient.post(@api, payload, { :params => params, :cookies => @cookies })
		@cookies.merge!(response.cookies)
		JSON.parse(response)
	end

	def login(user, pass, domain = nil)
		params = { :action => :login, :lgname => user, :lgpassword => pass }
		params.merge!({ :domain => domain }) unless domain.nil?

		data = post(params)
		if data['login']['result'] == 'NeedToken'
			params.merge!({ :lgtoken => data['login']['token'] })
			data = post(params)
		end
		return data
	end

	def logout
		post({ :action => :logout })
	end

	def query(params = {})
		post(params.merge({ :action => :query }))
	end

	def expandetemplates
	end
	def parse
	end
	def opensearch
	end
	def feedcontributions
	end
	def feedwatchlist
	end
	def help
	end
	def paraminfo
	end
	def rsd
	end
	def compare
	end
	def tokens
	end
	def purge
	end
	def setnotificationtimestamp
	end
	def rollback
	end
	def delete_by_title(titles = [], params = {})
		data = query({:prop => :info, :inprop => '', :intoken => :delete, :titles => titles.join('|') })
		_delete(data, params)
	end
	def delete_by_id(ids = [], params = {})
		data = query({:prop => :info, :inprop => '', :intoken => :delete, :pageids => ids.join('|') })
		_delete(data, params)
	end
	def _delete(data, params)
		data['query']['pages'].each do |id,page|
			print "Deleting page: #{page['title']}\n"
			post(params.merge({ :action => :delete, :pageid => id, :token => page['deletetoken'] }))
		end
	end
	def undelete
	end
	def protect
	end
	def block(users = [], params = {})
		data = query({:prop => :info, :intoken => :block, :titles => users.map{|u| "User:#{u}"}.join('|') })
		data['query']['pages'].each do |id,userpage|
			print "Blocking #{userpage['title']}\n"
			post(params.merge({
						:action => :block,
						:user => userpage['title'].sub(/^User:/,''),
						:token => userpage['blocktoken'],
					}))
		end
	end
	def unblock
	end
	def move
	end
	def edit
	end
	def upload
	end
	def filerevert
	end
	def emailuser
	end
	def watch
	end
	def patrol(params = {})
		post(params.merge({:action => :patrol}))
	end
	def import
	end
	def userrights
	end
	def options
	end
end
