#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

load 'mwapi.rb'
require 'yaml'

mw = MWApi.new('https://wiki.parabolagnulinux.org/api.php')
credentials = YAML.load_file('credentials.yml')
mw.login(credentials['username'], credentials['password'])

apcontinue = ''
while not apcontinue.nil? do
	data = mw.query(:list => :allpages, :apnamespace => 2, :aplimit => 200, :apcontinue => apcontinue)
	for page in data['query']['allpages']
		print "#{page['title']}\n"
	end
	if data['query-continue'].nil?
		apcontinue = nil
	else
		apcontinue = data['query-continue']['allpages']['apcontinue']
	end
end
