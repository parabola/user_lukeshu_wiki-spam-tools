#!/bin/bash

cd user-pages.cache
for file in *; do
	printf '%s\t%s\n' \
		"$file" \
		"$(< "$file" sed -n '$p' | sed -r 's|<br\s*/?>|\n|g' | sed -n '$p')"
done
